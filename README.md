<div align="center">
  <h2>WEB APIs</h2>
  <p>Los datos se convirtieron en la nueva materia prima de los negocios. <b>Privacidad y Protección de Datos. #DataPrivacity</b></p>
</div>

<div align="center">
  <a href="https://dataprivacity.com/" target="_blank">
    <img src="assets/images/logo.png" width="200">
  </a>
  <h1>@DataPrivacity</h1>
</div>

<div align="center">
  <p>Aprende en nuestras redes:</p>
  <a href="https://DataPrivacity.com/" target="_blank">
    <img src="assets/images/globe.svg" width="50">
  </a>
  <a href="https://twitter.com/DataPrivacity" target="_blank">
    <img src="assets/images/twitter.svg" width="50">
  </a>
  <a href="https://instagram.com/DataPrivacity" target="_blank">
    <img src="assets/images/instagram.svg" width="50">
  </a>
  <a href="https://www.facebook.com/DataPrivacity/" target="_blank">
    <img src="assets/images/facebook.svg" width="50">
  </a>
  <a href="https://youtube.com/" target="_blank">
    <img src="assets/images/youtube.svg" width="50">
  </a>
  <a href="https://linkedin.com/company/DataPrivacity" target="_blank">
    <img src="assets/images/linkedin.svg" width="50">
  </a>
</div>

<div align="center">
  <p>Alojamos proyectos en:</p>
  <a href="https://gitlab.com/DataPrivacity" target="_blank">
    <img src="assets/images/gitlab.svg" width="50">
  </a>
  <a href="https://github.com/DataPrivacity" target="_blank">
    <img src="assets/images/github.svg" width="50">
  </a>
    <a href="https://drive.google.com/drive/folders/1uHOoUbx83PSKySfbxBOqwlthUA6ofHYP?usp=sharing" target="_blank">
      <img src="assets/images/download.svg" width="50">
  </a>
</div>

<div align="center">
  <p>DR, Cofundador de: DataPrivacity</p>
  <a href="https://twitter.com/soyceros" target="_blank">
    <img src="assets/images/cofundador.png" width="50">
  </a>
</div>

Todo el contenido publicado es modificable, si tu quieres colaborar, ves errores o añadir contenido, puedes hacerlo; cumpliendo los siguientes requisitos:

<div align="center">
  <b>“Todos para uno, uno para todos.”</b>
</div>
<br>
<div align="center">
  <b>“Llegar juntos es el principio; mantenerse juntos es el progreso; trabajar juntos es el éxito.”</b>
</div>
<br>

<div align="center">
  <b>Mi trabajo en el software libre está motivado por un objetivo idealista: difundir libertad y cooperación. Quiero motivar la expansión del software libre, reemplazando el software privativo que prohíbe la cooperación, y de este modo hacer nuestra sociedad mejor. Richard Stallman</b>
</div>

# Tabla de contenido
- [Introducción](#Introducción)
    - [APIs](#APIs)
    - [WEB APIs](#WEB-APIs)
    - [window](#window)
    - [Nodo](#Nodo)
    - [NodeLists vs Array](#NodeLists-vs-Array)
- [Document Object](#Document-Object)
    - [¿Qué es el Document Object?](#¿Qué-es-el-Document-Object?)
- [Propiedades y métodos](#Propiedades-y-métodos)
    - [Nodos](#Nodos)
    - [Atributos](#Atributos)
    - [Estilos](#Estilos)
    - [Variables](#Variables)
    - [Atributos](#Atributos)
    - [Remplazando contenido](#Remplazando-contenido)
    - [Recorriendo el DOM](#Recorriendo-el-DOM)
    - [Creando Elementos y Fragmentos](#Creando-Elementos-y-Fragmentos)
- [Eventos](#Eventos)
    - [¿Qué es un evento?](#¿Qué-es-un-evento?)
    - [Manejador de evento](#Manejador-de-evento)
    - [Eventos con parámetros](#Eventos-con-parámetros)
    - [Remover-eventos](#Remover-eventos)
    - [Propagación de eventos](#Propagación-de-eventos)
    - [Delegación de eventos](#Delegación-de-eventos)

# <a name="Introducción">Introducción</a>
## <a name="APIs">APIs</a>

API significa “interfaz de programación de aplicaciones”. En el contexto de las API, la palabra aplicación se refiere a cualquier software con una función distinta. La interfaz puede considerarse como un contrato de servicio entre dos aplicaciones. Este contrato define cómo se comunican entre sí mediante solicitudes y respuestas. La documentación de su API contiene información sobre cómo los desarrolladores deben estructurar esas solicitudes y respuestas.

## <a name="WEB-APIs">WEB APIs</a>

| Contenido | Definicion |
| ------ | ------ |
| DOM: Document Object Model | El DOM (Document Object Model) es una API que representa e interactúa con cualquier documento HTML o XML . El DOM es un modelo de documento cargado en el navegador y que representa el documento como un árbol de nodos, donde cada nodo representa parte del documento (por ejemplo, un elemento , una cadena de texto o un comentario). El DOM es una de las API más utilizadas en la Web porque permite que el código que se ejecuta en un navegador acceda e interactúe con cada nodo del documento. Los nodos se pueden crear, mover y cambiar. Los detectores de eventos pueden agregarse a los nodos y activarse cuando se produce un evento determinado. |
| CSSOM: CSS Object Model | El Modelo de objetos CSS (CSS Object Model) es un conjunto de APIs que permite manipular CSS desde JavaScript. Así como el DOM (Document Object Model) es para HTML, el CSSOM (CSS Object Model) es para CSS. Permite leer y modificar el estilo de CSS de forma dinámica. |
| BOM: Browser Object Model | Las versiones 3.0 de los navegadores Internet Explorer y Netscape Navigator introdujeron el concepto de Browser Object Model o BOM, que permite acceder y modificar las propiedades de las ventanas del propio navegador. Mediante BOM, es posible redimensionar y mover la ventana del navegador, modificar el texto que se muestra en la barra de estado y realizar muchas otras manipulaciones no relacionadas con el contenido de la página HTML. |
| Eventos | ---- |
| Forms | ---- |
| AJAX - Fetch | ---- |
| History | ---- |
| Web Storage | ---- |
| Geolocation | ---- |
| Drag & Drop | ---- |
| Indexed DB | ---- |
| Canvas | ---- |
| MatchMedia | ---- |
| SpeechSynthesis | ---- |

## <a name="window">window</a>

El objeto window representa la ventana que contiene un documento DOM; la propiedad document apunta al DOM document cargado en esa ventana. El objeto window al que pertenece un documento puede ser obtenido usando la propiedad document.defaultView.

Esta sección proporciona una pequeña referencia a todos los métodos, propiedades y eventos disponibles a través del objeto DOM window. El objeto window implementa la interfaz Window , que a su vez hereda de la interfaz AbstractView. Algunas funciones como globales adicionales, espacios de nombres, interfaces, y constructores no típicamente asociados con el objeto window pero disponibles en éste, están listados en las Referencia de JavaScript y en el Referencia DOM de Gecko.

Object(Objeto global) que es igual a window. También se genera una variable this. This depende del contexto, en este caso en el entorno Global Object(Objeto global) this es igual al Global Object(Objeto global). Del Global Object(Objeto global) que es igual a window. De window cuelgan todas las APIs del Navegador.

<div align="center">
  <img src="assets/images/web_apis/window.png" width="1000">
</div>

```js
-- ############################################################################################ --
-- -------------------------------------------------------------------------------------------- --

console.log(window);

-- -------------------------------------------------------------------------------------------- --

console.log(this);

-- -------------------------------------------------------------------------------------------- --

window === this

-- -------------------------------------------------------------------------------------------- --
-- ############################################################################################ --
```

- [window](#https://dev.to/snickdx/how-well-do-you-know-dom-javascript-3fl5)

## <a name="Nodo">Nodo</a>

El término nodo puede tener varios significados según el contexto. Puede referirse a: En el contexto del DOM, un nodo es un único punto en el arbol de nodos. Los nodos pueden ser varias cosas el documento mismo, elementos, texto y comentarios.

#### Node.nodeType

La Node.nodeType propiedad que devuelve un número entero que identifica cuál es el nodo.. Distingue diferentes tipos de nodos entre sí, como elements, text y comments.

```js
-- ############################################################################################ --
-- -------------------------------------------------------------------------------------------- --

// Sintaxis
nodo.nodeType
// Ejemplo
let nodo = document.head;
nodo.nodeType;
// output
// 1

-- -------------------------------------------------------------------------------------------- --
-- ############################################################################################ --
```

| Constante | Valor entero que identifica cuál es el nodo | Descripción |
| ------ | ------ | ------ |
| Node.ELEMENT_NODE | 1 | Un Elemento como p o div |
| Node.ATTRIBUTE_NODE | 2 | Un atributo de un Elemento |
| Node.TEXT_NODE | 3 | El texto interior de una Elemento o atributo |
| Node.CDATA_SECTION_NODE | 4 | Vea al enlase de avajo he investiga |
| Node.PROCESSING_INSTRUCTION_NODE | 7 | Vea al enlase de avajo he investiga |
| Node.COMMENT_NODE | 8 | Vea al enlase de avajo he investiga |
| Node.DOCUMENT_NODE | 9 | 	Vea al enlase de avajo he investiga |
| Node.DOCUMENT_TYPE_NODE | 10 | Vea al enlase de avajo he investiga |
| Node.DOCUMENT_FRAGMENT_NODE | 11 | 	Vea al enlase de avajo he investiga |

- [Nodos](https://developer.mozilla.org/en-US/docs/Web/API/Node/nodetype)

## <a name="NodeLists-vs-Array">NodeLists vs Array</a>

| Contenido | Definicion |
| ------ | ------ |
| NodeList | puede parecer un Array, pero en realidad, ambos son dos cosas completamente diferentes. Un NodeListobjeto es básicamente una colección de nodos DOM extraídos del documento HTML. Aunque puede acceder a los elementos de NodeList, como un Array y usar la misma propiedad length, aún existen ciertas diferencias. No puede usar los métodos de Array comunes como push(), pop(), slice() y join() directamente en un NodeList. Primero debe convertir NodeLista en un Array normal usando el método Array.from(). |
| Array | Un Array es un tipo de datos especial en JavaScript, que puede almacenar una colección de elementos arbitrarios. |

#### NodeList

Para crear un NodeList objeto, puede utilizar el método querySelectorAll. El siguiente ejemplo selecciona todos los elementos p en el documento:

```js
-- ############################################################################################ --
-- -------------------------------------------------------------------------------------------- --

let elementos = document.querySelectorAll("p");
console.log(`Total de elementos: ${elementos.length}`);

-- -------------------------------------------------------------------------------------------- --
-- ############################################################################################ --
```

#### Array

Se puede crear un Array de JavaScript utilizando la sintaxis literal del Array o el constructor Array

```js
-- ############################################################################################ --
-- -------------------------------------------------------------------------------------------- --

let elementos = ["Pizza", "Pasta", "Burger", "Cake"];
console.log(`Total de elementos: ${elementos.length}`);
// O
let elementos = new Array("Pizza", "Pasta", "Burger", "Cake");
console.log(`Total de elementos: ${elementos.length}`);

-- -------------------------------------------------------------------------------------------- --
-- ############################################################################################ --
```

#### Convertir un NodeList a array

```js
-- ############################################################################################ --
-- -------------------------------------------------------------------------------------------- --

const elementos = document.querySelectorAll("p");
const miArray = [...elementos];

// Sin embargo, está forma es más legible:

const elementos = document.querySelectorAll("p");
const miArray = Array.from(elementos);

-- -------------------------------------------------------------------------------------------- --
-- ############################################################################################ --
```

#### Ejemplos

```js
-- ############################################################################################ --
-- -------------------------------------------------------------------------------------------- --

// forEach resive un funcion anonima como parametro

const elementos = document.querySelectorAll("p");
const miArray = Array.from(elementos);

miArray.forEach(function(miArray){
  console.log(miArray);
});

-- -------------------------------------------------------------------------------------------- --

// forEach resive un funcion como parametro Arrow functions

const elementos = document.querySelectorAll("p");
const miArray = Array.from(elementos);

miArray.forEach( (elementos) => {
  console.log(elementos);
});

-- -------------------------------------------------------------------------------------------- --

// forEach resive un funcion como parametro Arrow functions

const elementos = document.querySelectorAll("p");
const miArray = Array.from(elementos);

miArray.forEach( elementos => console.log(elementos));

-- -------------------------------------------------------------------------------------------- --
-- ############################################################################################ --
```

- [NodeList](https://developer.mozilla.org/es/docs/Web/API/NodeList)

# <a name="Document-Object">Document Object</a>
## <a name="¿Qué-es-el-Document-Object?">¿Qué es el Document Object?</a>

La interfaz Document representa cualquier página web cargada en el navegador y sirve como punto de entrada al contenido de la página web, que es el árbol DOM (Document Object Model).

Cuando un documento HTML se carga en un navegador web, se convierte en un document object. El document object. es el nodo raíz del documento HTML. El document object, es una propiedad del window object.

```js
-- ############################################################################################ --
-- -------------------------------------------------------------------------------------------- --

// Se accede al document object con:
console.log(window.document);
// No es necesario poner window
console.log(document);

-- -------------------------------------------------------------------------------------------- --
-- ############################################################################################ --
```

# <a name="Propiedades-y-métodos">Propiedades y métodos</a>
## <a name="Nodos">Nodos</a>

```js
-- ############################################################################################ --
-- -------------------------------------------------------------------------------------------- --

// Devuelve el elemento raíz del documento html
console.log(document.documentElement);

-- -------------------------------------------------------------------------------------------- --

// Devuelve el elemento head
console.log(document.head);

-- -------------------------------------------------------------------------------------------- --

// Devuelve el elemento body
console.log(document.body);

-- -------------------------------------------------------------------------------------------- --

// Devuelve la Definición de Tipo de Documento
console.log(document.doctype);

-- -------------------------------------------------------------------------------------------- --

// Devuelve el caracter de nuestro documento
console.log(document.charset);

-- -------------------------------------------------------------------------------------------- --

// Establece u obtiene el título del documento
console.log(document.title);

-- -------------------------------------------------------------------------------------------- --

// Devuelve un HTMLCollection de los hipervínculos del documento.
console.log(document.links);

-- -------------------------------------------------------------------------------------------- --

// Devuelve un HTMLCollection de las imágenes del documento.
console.log(document.images);

-- -------------------------------------------------------------------------------------------- --

// Devuelve un HTMLCollection de los elementos <form> del documento.
console.log(document.forms);

-- -------------------------------------------------------------------------------------------- --

// Devuelve un objeto StyleSheetList de CSSStyleSheet para hojas de estilo explícitamente 
// vinculadas o incrustadas en un documento.
console.log(document.styleSheets);

-- -------------------------------------------------------------------------------------------- --

// Devuelve un HTMLCollection de los elementos <script> del documento.
console.log(document.scripts);

-- -------------------------------------------------------------------------------------------- --

// Devuelve un objeto Selection que representa el rango de texto seleccionado por el usuario o 
// la posición actual del signo de intercalación.

// Con el metodo .toString() convierte a string.

setTimeout(() => {
  console.log(document.getSelection().toString());
}, 2000);

-- -------------------------------------------------------------------------------------------- --

// Escribe texto en un documento.
// No es muy buena practica utilizarlo o no se deberia usarlo. Lo escrive justo despues que se 
// cierra la etiqueta script.
document.write("<h1>Hola Mundo desde el DOM</h1>");

-- -------------------------------------------------------------------------------------------- --
-- ############################################################################################ --
```

```js
-- ############################################################################################ --
-- -------------------------------------------------------------------------------------------- --
// Metodos que ya no se debe de usarse
-- -------------------------------------------------------------------------------------------- --

// Devuelve una HTMLCollection de elementos con el nombre de etiqueta dado.

// Sintaxis
document.getElementsByTagName("nombreEtiqueta");
// Ejemplo
document.getElementsByTagName("li");

-- -------------------------------------------------------------------------------------------- --

// Devuelve una HTMLCollection de elementos con el nombre de clase dado.

// Sintaxis
document.getElementsByClassName("nombreClase");
// Ejemplo
document.getElementsByClassName("descripcion");

-- -------------------------------------------------------------------------------------------- --

// Devuelve una NodeList de elementos con el nombre de Name dado.

// Sintaxis
document.getElementsByName("nombreName");
// Ejemplo
document.getElementsByName("mensaje");

-- ############################################################################################ --
-- -------------------------------------------------------------------------------------------- --
// Metodos nuevos que deberian de usarse
-- -------------------------------------------------------------------------------------------- --

// Devuelve una referencia de objeto con el nombre de identificador dado.

// Sintaxis
document.getElementById("nombreIdentificador");
// Ejemplo
document.getElementById("mensajePago");

-- -------------------------------------------------------------------------------------------- --

// Devuelve el primer nodo Element dentro del documento, en el orden del documento, que coincide 
// con el nombre de selectores especificados.

// Sintaxis
document.querySelector("#nombreIdentificador");
// Ejemplo
document.querySelector("#mensajePago");

// Sintaxis
document.querySelector(".nombreClase");
// Ejemplo
document.querySelector(".descripcion");

// Sintaxis
document.querySelector("nombreEtiqueta");
// Ejemplo
document.querySelector("h1");

// Recoriendo nodos para encontrar mas facil el Element dentro del documento
// Sintaxis
document.querySelector("nodoPadre nodoHijo");
// Ejemplo
document.querySelector(".sectionGaleria .descripcion");

-- -------------------------------------------------------------------------------------------- --

// Devuelve una NodeList de todos los nodos Element dentro del documento que coinciden con 
// el nombre de selectores especificados.

// Sintaxis
document.querySelectorAll("#nombreIdentificador");
// Ejemplo
document.querySelectorAll("#mensajePago");

// Sintaxis
document.querySelectorAll(".nombreClase");
// Ejemplo
document.querySelectorAll(".descripcion");

// Sintaxis
document.querySelectorAll("nombreEtiqueta");
// Ejemplo
document.querySelectorAll("a");

// Indicar el indice que queremos que devuelva
// Sintaxis
document.querySelectorAll("#nombreIdentificador || .nombreClase || nombreEtiqueta")[indice];
// Ejemplo
document.querySelectorAll("a")[5];

// Recoriendo nodos para encontrar mas facil todos los nodos Element dentro del documento
// Sintaxis
document.querySelectorAll("nodoPadre nodoHijo");
// Ejemplo
document.querySelectorAll("ul li");

-- ############################################################################################ --
-- -------------------------------------------------------------------------------------------- --

// metodo .length para contar cuantos elementos tiene mi NodeList
document.querySelectorAll("li").length;

// metodo .forEach() para recorrer nuestros elementos tiene mi NodeList
document.querySelectorAll("li").forEach(function(articulo){
  console.log(articulo);
});

-- -------------------------------------------------------------------------------------------- --
-- ############################################################################################ --
```

- [Propiedades y métodos](https://developer.mozilla.org/es/docs/Web/API/Document)

## <a name="Atributos">Atributos</a>

```js
-- ############################################################################################ --
-- -------------------------------------------------------------------------------------------- --
// Formas de acceder a los atributos
-- -------------------------------------------------------------------------------------------- --

// Devuelve el valor del atributo especificado
// Sintaxis
.nombreAtributo
// Ejemplo
document.documentElement.lang

-- -------------------------------------------------------------------------------------------- --

// Devuelve el valor del atributo especificado
// Sintaxis
.getAttribute("nombreAtributo");
// Ejemplo
document.documentElement.getAttribute("lang");
// Ejemplo
document.querySelector(".descripcion").getAttribute("id");

-- -------------------------------------------------------------------------------------------- --

// Devuelve el valor del atributo especificado
// Sintaxis
.nombreAtributo
// Ejemplo
document.querySelector(".descripcion").id;

-- ############################################################################################ --
-- -------------------------------------------------------------------------------------------- --
// Algunas diferencias
-- -------------------------------------------------------------------------------------------- --

document.querySelector(".link").href;
// versus
document.querySelector(".link").getAttribute("href");

-- ############################################################################################ --
-- -------------------------------------------------------------------------------------------- --
// Formas de establecer nuevos valores y nuevos atributos
-- -------------------------------------------------------------------------------------------- --

// Sintaxis
.nombreAtributo = "nuevoValor";
// Ejemplo
document.documentElement.lang = "Hola, mundo";

-- -------------------------------------------------------------------------------------------- --

// Sintaxis
.nombreAtributo = "nuevoValor";
// Ejemplo
document.querySelector(".descripcion").id = "Hola, mundo";

-- -------------------------------------------------------------------------------------------- --

// Sintaxis
.setAttribute("nombreAtributo", "nuevoValor");
// Ejemplo
document.querySelector(".descripcion").setAttribute("id", "Hola, mundo");

-- -------------------------------------------------------------------------------------------- --

// Sintaxis
.setAttribute("nuevoAtributo", "nuevoValor");
// Ejemplo
document.querySelector(".link").setAttribute("target", "_black");

-- ############################################################################################ --
-- -------------------------------------------------------------------------------------------- --
// validar si existe un atributo
// Devuelve un true si existe o un false si no existe
-- -------------------------------------------------------------------------------------------- --

// Sintaxis
.hasAttribute("nombreAtributo");
// Ejemplo
document.querySelector(".input").hasAttribute("placeholder");

-- -------------------------------------------------------------------------------------------- --
// Eliminar un atributo
-- -------------------------------------------------------------------------------------------- --

.removeAttribute("nombreAtributo");
// Ejemplo
document.querySelector(".input").removeAttribute("placeholder");

-- -------------------------------------------------------------------------------------------- --
-- ############################################################################################ --
```

#### Convención al guardar un Nodo en una variable

```js
-- ############################################################################################ --
-- -------------------------------------------------------------------------------------------- --

const $nombreVariable = document.querySelector(".descripcion");

-- -------------------------------------------------------------------------------------------- --
-- ############################################################################################ --
```

## <a name="Estilos">Estilos</a>

```js
-- ############################################################################################ --
-- -------------------------------------------------------------------------------------------- --
// Hacceder a los estilos en linea que tenga un Elemento
-- -------------------------------------------------------------------------------------------- --

// Haceder a las propiedades dinamicas o Compute. Estilos que tienen los elementos dados 
// por el navegadores
const $titulo = document.querySelector(".link");
window.getComputedStyle($titulo)

// Hacceder a las propiedades dinamicas o Compute, devolver un valor en particular de nuestro 
// atributo style
const $link = document.querySelector(".link");
getComputedStyle($link).getPropertyValue("background");

// Devuelve un CSSStyleDeclaration de todas las propiedades de css validos del elemento dado
// Sintaxis
.style;
// Ejemplo
document.querySelector(".link").style;

// Devuelve el valor del atributo style
// Sintaxis
.getAttribute("style");
// Ejemplo
document.querySelector(".link").getAttribute("style");

// Devuelve un valor de la propiedad en particular de nuestro atributo
// Sintaxis
.style.nombrePropiedad;
// Ejemplo
document.querySelector(".link").style.backgroundColor;

// Agragamos el atributo style con valores a un elemento dado
// Sintaxis
.setAttribute("style", "nombrePropiedad: valor;");
// Ejemplo
document.querySelector("h1").setAttribute("style", "color: red;");

-- -------------------------------------------------------------------------------------------- --

// Establecer estilos en linea a un elemento
const $titulo = document.querySelector("h1");
$titulo.style.setProperty("width", "100%");
$titulo.style.setProperty("height", "200px");
$titulo.style.setProperty("font-size", "50px");
$titulo.style.setProperty("background", "black");
$titulo.style.setProperty("display", "flex");
$titulo.style.setProperty("align-items", "center");
$titulo.style.setProperty("justify-content", "center");
$titulo.style.setProperty("color", "red");
$titulo.style.textAlign = "center";
$titulo.style.borderRadius = ".5rem";

-- -------------------------------------------------------------------------------------------- --
-- ############################################################################################ --
```

## <a name="Variables">Variables</a>

```js
-- ############################################################################################ --
-- -------------------------------------------------------------------------------------------- --

// Hacedemos a las variables por la etiqueta root que es elemento html
const $html = document.documentElement;
let color = getComputedStyle($html).getPropertyValue("--rojo");
console.log(color);
// Aplicamos las variables
let $titulo = document.querySelector("h1");
$titulo.style.backgroundColor = color;

-- -------------------------------------------------------------------------------------------- --

// Modificar una variable
const $html = document.documentElement;
$html.style.setProperty("--rojo", "rgb(6, 68, 126)");
// Lo guardamos
let color = getComputedStyle($html).getPropertyValue("--rojo");
// Lo aplicamos de nuevo nuestra variables
let $titulo = document.querySelector("h1");
$titulo.style.setProperty("background-color", color);

-- -------------------------------------------------------------------------------------------- --
-- ############################################################################################ --
```

## <a name="Atributos">Atributos</a>

#### class

```js

<h1 class="mensaje titulo informacion">Hola, mundo</h1>

```

###### Leer

```js

// Lee todos los valores del atributo
// Sintaxis
.classList;
// Ejemplo
document.querySelector("h1").classList;

```

###### Obtener

```js

// Obtener todos los valores del atributo
// Sintaxis
.className;
// Ejemplo
document.querySelector("h1").className;

```

###### Validar

```js

// Validar si existe el valor del atributo
// Sintaxis
classList.contains("valor");
// Ejemplo
document.querySelector("h1").classList.contains("mensaje");

```

###### Crear

```js

// Crear un nuevo valor al atributo
// Sintaxis
classList.add("valor");
// Ejemplo
document.querySelector("h1").classList.add("descripcion");

```

###### Eliminar

```js

// Eliminar un valor del atributo
// Sintaxis
classList.remove("valor");
// Ejemplo
document.querySelector("h1").classList.remove("mensaje");

```

###### Crear o Eliminar

```js

// Si no exite el valor se lo crea al atributo
// Sintaxis
classList.toggle("valor");
// Ejemplo
document.querySelector("h1").classList.toggle("descripcion");

```

```js

// Si exite el valor se lo elimina al atributo
// Sintaxis
classList.toggle("valor");
// Ejemplo
document.querySelector("h1").classList.toggle("descripcion");

```

###### Modificar

```js

// Modificar un valor del atributo
// Sintaxis
.classList.replace("identificadorDescripcion", "nuevoValor");
// Ejemplo
document.querySelector("h1").classList.replace("mensaje", "mensajeError");

```

###### Crear varios valores a la vez

```js

// Crear varios valores a la vez del atributo
// Sintaxis
.classList.add("nuevoValor", "nuevoValor");
// Ejemplo
document.querySelector("h1").classList.add("descripcion", "codigo");

```

###### Eliminar varios valores a la vez

```js

// Eliminar varios valores a la vez del atributo
// Sintaxis
.classList.remove("valor", "valor");
// Ejemplo
document.querySelector("h1").classList.remove("mensaje", "titulo");

```

#### data attributes

```js

<h1 data-codigo="785452214" data-activo="1">Hola, mundo</h1>

```

###### Leer

```js

// Lee todos los valores de los atributos
// Sintaxis
.dataset;
// Ejemplo
document.querySelector("h1").dataset;

```

###### Obtener

```js

// Obtener el valor del atributo dado
// Sintaxis
.getAttribute("data-identificadorDescripcion");
// Ejemplo
document.querySelector("h1").getAttribute("data-codigo");

// Obtener el valor del atributo dado
// Sintaxis
.dataset.identificadorDescripcion;
// Ejemplo
document.querySelector("h1").dataset.codigo;

```

###### Modificar

```js

// Modificar el valor del atributo dado
// Sintaxis
.setAttribute("data-identificadorDescripcion", "nuevoValor");
// Ejemplo
document.querySelector("h1").setAttribute("data-codigo", "000");

// Modificar el valor del atributo dado
// Sintaxis
.dataset.identificadorDescripcion = "nuevoValor";
// Ejemplo
document.querySelector("h1").dataset.codigo = "0000";

```

###### Crear

```js

// Crear un nuevo atributo
// Sintaxis
.setAttribute("data-identificadorDescripcion", "valor");
// Ejemplo
document.querySelector("h1").setAttribute("data-descripcion", "Mensaje");

```

###### Validar

```js

// Validar si existe el atributo dado
// Sintaxis
.hasAttribute("data-identificadorDescripcion");
// Ejemplo
document.querySelector("h1").hasAttribute("data-codigo");

```

###### Eliminar

```js

// Eliminar el atributo dado
// Sintaxis
.removeAttribute("data-identificadorDescripcion");
// Ejemplo
document.querySelector("h1").removeAttribute("data-codigo");

```

## <a name="Remplazando-contenido">Remplazando contenido</a>

```js
-- ############################################################################################ --
-- -------------------------------------------------------------------------------------------- --

let $card = document.querySelector("#mensajePago");
let text = `
  <figure>
  <img src="https://placeimg.com/200/200/tech" alt="Tech">
  <figcaption>Tech</figcaption>
  </figure>`;

// Esto no es estandar
// $card.innerText = text;

// Estos si son estandar
// $card.textContent = text;
// $card.innerHTML = text;
$card.outerHTML = text;

-- -------------------------------------------------------------------------------------------- --
-- ############################################################################################ --
```

## <a name="Recorriendo-el-DOM">Recorriendo el DOM</a>

El documento actualmente cargado en cada una de las pestañas de su navegador está representado por un modelo de objeto de documento. Esta es una representación de "estructura de árbol" creada por el navegador que permite que los lenguajes de programación accedan fácilmente a la estructura HTML; por ejemplo, el navegador mismo la usa para aplicar estilo y otra información a los elementos correctos a medida que representa una página, y los desarrolladores como si pudiera manipular el DOM con JavaScript después de que se haya renderizado la página.

El DOM, se ve así:

<div align="center">
  <img src="assets/images/web_apis/recorriendo_el_dom.png" width="600">
</div>

Cada entrada en el árbol se llama nodo . Puede ver en el diagrama anterior que algunos nodos representan elementos (identificados como HTML, HEAD, META etc.) y otros representan texto (identificados como #text). También hay otros tipos de nodos , pero estos son los principales que encontrará.

También se hace referencia a los nodos por su posición en el árbol en relación con otros nodos:

| Referencia a los nodos por su posición | Descripcion |
| ------ | ------ |
| Nodo raíz | El nodo superior del árbol, que en el caso de HTML siempre es el HTML nodo. |
| Nodo hijo | Un nodo directamente dentro de otro nodo. Por ejemplo, IMG es un hijo de SECTION en el ejemplo anterior. |
| Nodo descendiente | Un nodo en cualquier lugar dentro de otro nodo. Por ejemplo, IMG es hijo de SECTION en el ejemplo anterior y también es descendiente. IMG no es hijo de BODY, ya que está dos niveles por debajo de él en el árbol, pero es descendiente de BODY |
| Nodo principal | Un nodo que tiene otro nodo dentro. Por ejemplo, BODY es el nodo principal de SECTION en el ejemplo anterior. |
| Nodos hermanos | Nodos que se encuentran en el mismo nivel en el árbol DOM. Por ejemplo, IMG y P son hermanos en el ejemplo anterior. |

#### Recorriendo el DOM

El DOM nos permite hacer cualquier cosa con sus elementos y contenidos, pero lo primero que tenemos que hacer es llegar al objeto correspondiente del DOM.

Todas las operaciones en el DOM comienzan con el objeto document. Este es el principal “punto de entrada” al DOM. Desde ahí podremos acceder a cualquier nodo. Esta imagen representa los enlaces que nos permiten viajar a través de los nodos del DOM:

<div align="center">
  <img src="assets/images/web_apis/recorriendo_el_dom1.png" width="600">
</div>

#### Los tres nodos superiores están disponibles como propiedades de document:

```js

let $html = document.documentElement

let $body = document.body

let $head = document.head

```

#### Hijos: childNodes, firstChild, lastChild

```js

let $nodoPrincipal = document.querySelector("section");
// Verificamos el tipo de NODO
console.log($nodoPrincipal.nodeType);

// Colección de hijos directos
console.log($nodoPrincipal.childNodes);

```

```js

let $nodoPrincipal = document.querySelector("section");
// Verificamos el tipo de NODO
console.log($nodoPrincipal.nodeType);

// Primer hijo
console.log($nodoPrincipal.firstChild);
// Verificamos el tipo de NODO
console.log($nodoPrincipal.firstChild.nodeType);

```

```js

let $nodoPrincipal = document.querySelector("section");
// Verificamos el tipo de NODO
console.log($nodoPrincipal.nodeType);

// Ultimo hijo
console.log($nodoPrincipal.lastChild);
// Verificamos el tipo de NODO
console.log($nodoPrincipal.lastChild.nodeType);

```

#### Hermanos y el padre

Los hermanos son nodos que son hijos del mismo padre. Por ejemplo, aquí head y body son hermanos:

```html

<html>
  <head>...</head><body>...</body>
</html>

```

- body se dice que es el hermano “siguiente” o a la “derecha” de head
- head se dice que es el hermano “anterior” o a la “izquierda” de body.

```js

// El padre está en la propiedad parentNode
console.log(document.body.parentNode);
// Verificamos el tipo de NODO
console.log(document.body.parentNode.nodeType);

```

```js

// El hermano siguiente está en la propiedad nextSibling. Después de head va body
console.log(document.head.nextSibling);
// Verificamos el tipo de NODO
console.log(document.head.nextSibling.nodeType);

```

```js

// El hermano anterior está en la propiedad previousSibling. Antes de body va head
console.log(document.body.previousSibling);
// Verificamos el tipo de NODO
console.log(document.body.previousSibling.nodeType);

```

#### Navegación solo por elementos

Las propiedades de navegación enumeradas abajo se refieren a todos los nodos. Por ejemplo, en childNodes podemos ver nodos de texto, nodos elementos; y si existen, incluso los nodos de comentarios.

Pero para muchas tareas no queremos los nodos de texto o comentarios. Queremos manipular el nodo que representa las etiquetas. Así que vamos a ver más enlaces de navegación que solo tienen en cuenta los elementos nodos:

```js

let $nodoPrincipal = document.querySelector("section");
// Verificamos el tipo de NODO
console.log($nodoPrincipal.nodeType);

// Colección de hijos directos
console.log($nodoPrincipal.children);

// Devuelve un hijo en particular segun el indice indicado
console.log($nodoPrincipal.children[1]);

```

```js

let $nodoPrincipal = document.querySelector("section");
// Verificamos el tipo de NODO
console.log($nodoPrincipal.nodeType);

// Primer hijo
console.log($nodoPrincipal.firstElementChild);

```

```js

let $nodoPrincipal = document.querySelector("section");
// Verificamos el tipo de NODO
console.log($nodoPrincipal.nodeType);

// Ultimo hijo
console.log($nodoPrincipal.lastElementChild);

```

```js

let $nodoPrincipal = document.querySelector("head");
// Verificamos el tipo de NODO
console.log($nodoPrincipal.nodeType);

// El hermano siguiente. Después de head va body
console.log($nodoPrincipal.nextElementSibling);

```

```js

let $nodoPrincipal = document.querySelector("body");
// Verificamos el tipo de NODO
console.log($nodoPrincipal.nodeType);

// El hermano anterior. Antes de body va head
console.log($nodoPrincipal.previousElementSibling);

```

```js

let $nodoPrincipal = document.querySelector("section");
// Verificamos el tipo de NODO
console.log($nodoPrincipal.nodeType);

// El padre
console.log($nodoPrincipal.parentElement);

```

Notas

```js

let $nodoPrincipal = document.querySelector("section");
// Verificamos el tipo de NODO
console.log($nodoPrincipal.nodeType);

// Metodo closest() buzca un padre indicado posteriormente
console.log($nodoPrincipal.closest("body"));

```

- [Recorriendo el DOM](https://es.javascript.info/dom-navigation)

## <a name="Creando-Elementos-y-Fragmentos">Creando Elementos y Fragmentos</a>

```js
-- ############################################################################################ --
-- -------------------------------------------------------------------------------------------- --

let $cards = document.querySelector(".cards");

// Creamos Nodos de tipo elementos con createElement()
let $figure = document.createElement("figure");
let $img = document.createElement("img");
let $figcaption = document.createElement("figcaption");

// Creamos un Nodo de tipo texto con createTextNode()
let $figcaptionText = document.createTextNode("Animals");

// Creamos atributos con su valor a los elementos indicados
$img.setAttribute("src", "https://placeimg.com/200/200/animals");
$img.setAttribute("alt", "Animals");
$figure.classList.add("card");

// Agregamnos los Nodos de tipo elementos al DOM. Lo agrega como un hijo y sera el ultimo
// del elemento padre dado, con el metodo appendChild()
$cards.appendChild($figure);
$figure.appendChild($img);
$figure.appendChild($figcaption);
$figcaption.appendChild($figcaptionText);

-- -------------------------------------------------------------------------------------------- --
// Este ejemplo no necesariamente es correcta pero si funciona
-- -------------------------------------------------------------------------------------------- --

let $cards = document.querySelector(".cards");

// Creamos Nodos de tipo elementos con createElement()
let $figure = document.createElement("figure");

// Agregamnos los Nodos de tipo elementos al DOM. Lo agrega como un hijo y sera el ultimo
// del elemento padre dado, con el metodo innerHTML, como tal no estamos creando un 
// Nodo de tipo elemento
$figure.innerHTML = `
<img src="https://placeimg.com/200/200/people" alt="People">
<figcaption>People</figcaption>
`;
$cards.appendChild($figure);

-- ############################################################################################ --
-- -------------------------------------------------------------------------------------------- --

let $cards = document.querySelector(".cards");
console.log($cards);

let $titulo = document.createElement("h1");
let $text = document.createTextNode("Animals");
console.log($text.nodeType);

$cards.appendChild($titulo);
$titulo.appendChild($text);

-- ############################################################################################ --
-- -------------------------------------------------------------------------------------------- --

let $contenedor = document.querySelector("body");

const estaciones = ["Estaciones del año", "Primavera", "Verano", "Otoño", "Invierno"];

// Creamos un Nodo de tipo elementos con createElement()
const $ul = document.createElement("ul");

// Agregamnos el Nodo de tipo elementos al DOM. Lo agrega como un hijo y sera el ultimo
// del elemento padre dado, con el metodo appendChild()
$contenedor.appendChild($ul);

estaciones.forEach((el) => {
  // Creamos un Nodos de tipo elementos con createElement()
  const $li = document.createElement("li");

  // No necesitamos crear un Nodo de tipo texto con createTextNode() simplemente haccedemos 
  // a la propiedad con textContent para asignarle el nuevo valor de cada elemento de 
  // nuestro arreglo
  $li.textContent = el;

  // Agregamnos el Nodo de tipo elementos al DOM. Lo agrega como un hijo y sera el ultimo
  // del elemento padre dado, con el metodo appendChild()
  $ul.appendChild($li);
});

-- ############################################################################################ --
-- -------------------------------------------------------------------------------------------- --

let $contenedor = document.querySelector("body");

const continentes = ["Continentes del Mundo", "África", "América", "Asia", "Europa", "Oceanía"];

// Creamos un Nodo de tipo elementos con createElement()
const $ul = document.createElement("ul");

// Agregamnos el Nodo de tipo elementos al DOM. Lo agrega como un hijo y sera el ultimo
// del elemento padre dado, con el metodo appendChild()
$contenedor.appendChild($ul);

// Agregamnos los Nodos de tipo elementos al DOM. Lo agrega como un hijo y sera el ultimo
// del elemento padre dado, con el metodo innerHTML, como tal no estamos creando un 
// Nodo de tipo elemento
continentes.forEach((el) => ($ul.innerHTML += `<li>${el}</li>`));

-- ############################################################################################ --
-- -------------------------------------------------------------------------------------------- --
// Inserccion por Fragmentos
// Mejorando el rendimiento de nuestra aplicacion cuando insertamos o mostramos
// miles de registros al DOM
-- -------------------------------------------------------------------------------------------- --

let $contenedor = document.querySelector("body");

const meses = [
"Meses del Año",
"Enero",
"Febrero",
"Marzo",
"Abril",
"Mayo",
"Junio",
"Julio",
"Agosto",
"Septiembre",
"Octubre",
"Noviembre",
"Diciembre"
];

// Creamos un Nodo de tipo elementos con createElement()
const $ul = document.createElement("ul");

// Creamos un Nodo de tipo fragmentos con createDocumentFragment()
const $fragment = document.createDocumentFragment();
// Verificar el tipo de Nodo
$fragment.nodeType

meses.forEach((el) => {
  // Creamos un Nodo de tipo elementos con createElement()
  const $li = document.createElement("li");
  
  // No necesitamos crear un Nodo de tipo texto con createTextNode() simplemente haccedemos 
  // a la propiedad con textContent para asignarle el nuevo valor de cada elemento de 
  // nuestro arreglo
  $li.textContent = el;

  // Agregamnos el Nodo de tipo elementos a nuestro fragmento. Lo agrega como un hijo y sera 
  // el ultimo del elemento padre dado, con el metodo appendChild()
  $fragment.appendChild($li);
});

// Agregamos nuestro fragmento
$ul.appendChild($fragment);
// Agregamnos el Nodo de tipo elementos al DOM. Lo agrega como un hijo y sera el ultimo
// del elemento padre dado, con el metodo appendChild()
$contenedor.appendChild($ul);

-- -------------------------------------------------------------------------------------------- --
-- ############################################################################################ --
```

# <a name="Eventos">Eventos</a>
## <a name="¿Qué-es-un-evento?">¿Qué es un evento?</a>

Es algo que ocurre. Generalmente los eventos ocurren cuando el usuario interactúa con el documento, pero podrían producirse por situaciones ajenas al usuario, como una imagen que no se puede cargar porque esté indisponible.

## <a name="Manejador-de-evento">Manejador de evento</a>

Para poder reaccionar a estos eventos, necesitamos incluir lo que llamaremos un manejador del evento (handler), que será una función que se ejecuta en caso de que el evento sea activado. Por lo que los manejadores de eventos son los que nos permitirán ejecutar código JavaScript.

<div align="center">
  <img src="assets/images/web_apis/eventos_javascript.png" width="1000">
</div>

## <a name="Eventos-con-parámetros">Eventos con parámetros</a>

#### Error

```js

function saludar(nombre = "Data"){
  alert(`Hola ${nombre}`);
}

$0.addEventListener("click", saludar);

```

#### Exito

```js

function saludar(nombre = "Data"){
  alert(`Hola ${nombre}`);
}

$0.addEventListener("click", function(){
  saludar();
});

```

```js

function saludar(nombre = "Data"){
  alert(`Hola ${nombre}`);
}

$0.addEventListener("click", ()=>{
  saludar();
  saludar("Privacity");
});

```

## <a name="Remover-eventos">Remover eventos</a>

```js

// Agrego el evento
node.addEventListener

// Quito el evento
node.removeEventListener

```

```js

function prueba(e){
  alert(`Soy una prueba ${e.type}`);
  $elemento.removeEventListener("dblclick", prueba);
}

const $elemento = $0;
$elemento.addEventListener("dblclick", prueba);

```

## <a name="Propagación-de-eventos">Propagación de eventos</a>

La propagación de eventos es un mecanismo que define cómo los eventos se propagan o viajan a través del árbol DOM para llegar a su destino.

#### Burbuja

En esta fase, el evento se propaga o hace burbujas en el árbol DOM, desde el elemento de destino hasta Window, visitando todos los ancestros del elemento de destino uno por uno.

Cuando un evento ocurre en un elemento, este primero ejecuta el manejador de evento que tiene asignado, luego los manejadores de su padre, y así hasta otros ancestros. Tenemos 3 elementos anidados section > article > p con un manejador de evento en cada uno de ellos. Así si hacemos clic en **p**, entonces veremos 3 alertas: **p**, **article** y **section**.

Este proceso se conoce como propagación porque los eventos se propagan desde el elemento más al interior, a través de los padres, como una burbuja en el agua.

```html

<section id="div1">
  1
  <article id="div2">
  2  
    <p id="div3">
    3
    </p>
  </article>
</section>

```

```js

const $div1 = document.getElementById("div1");
const $div2 = document.getElementById("div2");
const $div3 = document.getElementById("div3");

function saludar(e){
  alert(e.currentTarget.nodeName);
}

$div1.addEventListener("click", saludar);
$div2.addEventListener("click", saludar);
$div3.addEventListener("click", saludar);

```

<div align="center">
  <img src="assets/images/web_apis/burbuja.svg" width="500">
</div>

#### Captura

En esta fase, se propaga primero a través del evento padre, que es el objeto de window, luego el document, luego el html y luego los demás elementos internos. Baja hasta que llega al event.target (en lo que hiciste clic / el evento desencadenado).

<div align="center">
  <img src="assets/images/web_apis/captura.png" width="500">
</div>

## <a name="Delegación-de-eventos">Delegación de eventos</a>

Delegación de eventos es un mecanismo a través del cual evitamos asignar event listeners a múltiples nodos específicos del DOM, asignando un event listener a solo un nodo padre que contiene el resto de estos nodos.

La ventaja de usar delegación de eventos ya no es necesario detener la propagacion y en el caso hipotético de tener n elementos con la clase btn, solo hemos registrado un event listener para todos estos elementos, mientras que sin delegación de eventos debemos registrar n event listeners, es decir uno por cada nodo.