// -------------------------------------------------------------------------------------------------

// funcion manejador de evento
function holaMundo(){
  alert("hola, mundo");
  // Objeto del Manejador de evento 
  console.log(event);
}

// -------------------------------------------------------------------------------------------------

const $evento = document.getElementById("enviar");

// funcion manejador de evento
function holaPrueba(e){
  alert("hola, mundo");
  // Objeto del Manejador de evento 
  console.log(e);
  console.log(event);
}

$evento.onclick = holaPrueba;

// funcion anonima manejador de evento
$evento.onclick = function (e){
  alert("saludo");
  // Objeto del Manejador de evento
  console.log(e);
  console.log(event);
};

// -------------------------------------------------------------------------------------------------

const $cargar = document.getElementById("cargar");

// funcion manejador de evento
function saludo(e){
  alert("saludo");
  // Objeto del Manejador de evento 
  console.log(e);
  console.log(event);
}
$cargar.addEventListener("click", saludo);

// funcion anonima manejador de evento
$cargar.addEventListener("click", function(e){
  alert("Enviar");
  // Objeto del Manejador de evento 
  console.log(e.type);
  console.log(e.target);
  console.log(event);
});
