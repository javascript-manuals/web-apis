function saludar(e){
  alert(e.target.nodeName);
  console.log(e.target);
}

document.addEventListener("click", (e)=>{
  // console.log(e.target);

  if(e.target.matches(".caja")){
    saludar(e);
  }

  if(e.target.matches("button")){
    alert("Hola, mundo");
    console.log(e.target);
  }

});